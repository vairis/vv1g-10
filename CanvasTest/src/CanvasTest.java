/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//saakotneejais avots: http://www.egtry.com/java/awt/draw_save


import java.awt.Canvas;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
/**
 *
 * @author vairis
 */
public class CanvasTest {

    /**
     * @param args the command line arguments
     */

	public static void main(String[] args) {
		Frame f=new Frame("Draw shape and text on Canvas");
		 MyCanvas canvas=new MyCanvas();
		
		f.add(canvas);
		
		f.setSize(300,300);
		f.setVisible(true);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				canvas.saveCanvas(canvas);
				System.exit(0);
			}
		});

	}
    
}
class MyCanvas extends Canvas {
public void paint(Graphics g) {
		Graphics2D g2=(Graphics2D)g;
		g2.setBackground(Color.GREEN);
		g2.clearRect(0, 0, this.getWidth(), this.getHeight());
		g2.setColor(Color.BLACK);
		g2.drawString("Draw a rectangle", 100,100);
                g2.fillRect(200, 200, 50, 50);
		g2.drawRect(100,200,50,50);
                g2.drawOval(15, 19, 50, 100);
	}
	
	public static void saveCanvas(Canvas canvas) {

		BufferedImage image=new BufferedImage(canvas.getWidth(), canvas.getHeight(),BufferedImage.TYPE_INT_RGB);
		
		Graphics2D g2=(Graphics2D)image.getGraphics();
		
		
		canvas.paint(g2);
		try {
			ImageIO.write(image, "png", new File("/media/vairis/dat2/vv1g/canvas.png"));
		} catch (Exception e) {
			
		}
	}
}